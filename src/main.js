// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Main from '@/components/Main'
import router from './router'
import axios from 'axios'
import {Loading} from 'element-ui'
import pI from './assets/p.svg'
Vue.config.productionTip = false

Vue.prototype.$axios=axios;
Vue.prototype.$langAgnosticId='luckdraw';//活动编号

Vue.use(Loading);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  created() {
    this.server = 'https://junction.dev.havensphere.com/api/junctioncore/v1/';
    this.publicParams = '?hotel_id=0086000005&restaurant_id=r001&app_name=cts_mall_wechat&device_type=phone';
    this.$pI=pI;
  }
})
