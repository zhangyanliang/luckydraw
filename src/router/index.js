import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  // mode:'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: resolve=>require(['@/components/Main'],resolve)
    }
  ]
})

export default router
